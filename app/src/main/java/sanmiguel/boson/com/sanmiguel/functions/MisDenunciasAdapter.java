package sanmiguel.boson.com.sanmiguel.functions;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;

import sanmiguel.boson.com.sanmiguel.MainActivity;
import sanmiguel.boson.com.sanmiguel.R;
import sanmiguel.boson.com.sanmiguel.fragments.MisDenunciasFragment;
import sanmiguel.boson.com.sanmiguel.models.DetalleDenunciaModel;

/**
 * Created by Jarvis on 3/3/16.
 */
public class MisDenunciasAdapter extends BaseAdapter {

    MisDenunciasFragment main;
    //String BaseURL = "http://sanmiguel.gob.sv/alcaldiamovil/Files/";

    public MisDenunciasAdapter(MisDenunciasFragment main){
        this.main = main;
    }
    @Override
    public int getCount() {
        return  main.Denuncias.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static class ViewHolderItem {
        TextView name;
        TextView code;
        ImageView imageView2;
        TextView id;
        TextView fecha;
        TextView etapa;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        ViewHolderItem holder = new ViewHolderItem();
        if (convertView == null) {
            MainActivity activity = (MainActivity) main.getContext();

            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.cell_denuncias, null);

            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.code = (TextView) convertView.findViewById(R.id.code);
            holder.imageView2 = (ImageView) convertView.findViewById(R.id.imageView2);
            holder.id = (TextView) convertView.findViewById(R.id.id);
            holder.fecha = (TextView) convertView.findViewById(R.id.fecha);
            holder.etapa =   (TextView) convertView.findViewById(R.id.estado);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolderItem) convertView.getTag();
        }

        DetalleDenunciaModel current = this.main.Denuncias.get(position);

        holder.name.setText(current.getTipoDenuncia());
        holder.code.setText(current.getDescripcion());
        holder.id.setText(current.getId() + "");
        holder.fecha.setText("Fecha de ingreso de la solicitud: " + current.getFecha());
        if(current.getEtapa().equals("5") ){
            holder.etapa.setText("Estado: Solicitud finalizada");
        }else if(current.getEtapa().equals("6") ){
            holder.etapa.setText("Estado: Solicitud reportada como falsa");
        }else {
            holder.etapa.setText("Estado: Solicitud abierta");
            holder.etapa.setTextColor(Color.parseColor("#008D8A"));
        }

        String url = current.getPhotoURL(); //BaseURL + current.getId() + ".jpg";
        new DownloadImageTask((ImageView) holder.imageView2).execute(url);

        int width = Resources.getSystem().getDisplayMetrics().widthPixels;

        holder.imageView2.getLayoutParams().width = (width / 4);
        holder.imageView2.getLayoutParams().height = (width / 5);

        holder.imageView2.requestLayout();
        holder.imageView2.setScaleType(ImageView.ScaleType.FIT_XY);

        return convertView;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            if(result != null){
                bmImage.setBackground(null);
                bmImage.setImageBitmap(result);
            }

        }
    }
}
