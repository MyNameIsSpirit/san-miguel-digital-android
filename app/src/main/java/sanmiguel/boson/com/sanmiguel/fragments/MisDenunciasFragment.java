package sanmiguel.boson.com.sanmiguel.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import sanmiguel.boson.com.sanmiguel.MainActivity;
import sanmiguel.boson.com.sanmiguel.functions.DownloadData;
import sanmiguel.boson.com.sanmiguel.functions.MisDenunciasAdapter;
import sanmiguel.boson.com.sanmiguel.R;
import sanmiguel.boson.com.sanmiguel.models.DetalleDenunciaModel;

public class MisDenunciasFragment extends Fragment implements DownloadData.download_complete {

    private OnFragmentInteractionListener mListener;
    private String DenunciasResueltasURL;
    public ArrayList<DetalleDenunciaModel> Denuncias = new ArrayList<DetalleDenunciaModel>();
    public MisDenunciasAdapter adapter;
    public ListView list;
    private String androidId;
    private EditText cargando;

    public MisDenunciasFragment() {
        // Required empty public constructor
    }

    public static MisDenunciasFragment newInstance(String param1, String param2) {
        MisDenunciasFragment fragment = new MisDenunciasFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_mis_denuncias, container, false);

        Bundle data = getArguments();
        androidId = data.getString("deviceId");
        //androidId = androidId;

        list = (ListView) view.findViewById(R.id.list);
        cargando = (EditText) view.findViewById(R.id.cargando);
        adapter = new MisDenunciasAdapter(this);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               Integer selectedId =  Denuncias.get(position).getId();
                ((MainActivity) getContext()).goToDetalleDenuncia(selectedId);
            }
        });

        DownloadData download_data = new DownloadData((DownloadData.download_complete) this);
        download_data.download_data_from_link(getContext().getString(R.string.endpoint_mis_solicitudes) + androidId);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void get_data(String data) {
        try {
            JSONArray data_array = new JSONArray(data);

            if(data_array.length() == 0){
                cargando.setText("No se han encontrado solicitudes publicadas.");
            }else{
                cargando.setVisibility(View.GONE);
            }

            for (int i = 0 ; i < data_array.length() ; i++)
            {
                JSONObject obj = new JSONObject(data_array.get(i).toString());
                try {
                    DetalleDenunciaModel add = new DetalleDenunciaModel();
                    add.setTipoDenuncia(obj.getString("TipoProblema"));
                    add.setDescripcion(obj.getString("Descripcion"));
                    add.setPhotoURL(obj.getString("Foto"));
                    add.setFecha(obj.getString("Fecha"));
                    add.setEtapa(obj.getString("Etapa"));
                    add.setId(Integer.parseInt(obj.getString("Id")));
                    add.setLatitud(Double.parseDouble(obj.getString("Latitud")));
                    add.setLongitud(Double.parseDouble(obj.getString("Longitud")));

                    Denuncias.add(add);
                }catch(Exception ex){
                }
            }
            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
