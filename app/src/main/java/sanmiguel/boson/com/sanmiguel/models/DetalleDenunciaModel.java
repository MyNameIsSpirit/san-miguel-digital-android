package sanmiguel.boson.com.sanmiguel.models;

import java.util.List;

/**
 * Created by Jarvis on 2/27/16.
 */
public class DetalleDenunciaModel {

    private String TipoDenuncia;
    private String Descripcion;
    private String PhotoURL;
    private double Latitud;
    private double Longitud;
    private Integer Id;
    private String Fecha;
    private String Etapa;

    public String getEtapa() {
        return Etapa;
    }

    public void setEtapa(String etapa) {
        Etapa = etapa;
    }

    public List<ComentarioModel> getComentarios() {
        return Comentarios;
    }

    public void setComentarios(List<ComentarioModel> comentarios) {
        Comentarios = comentarios;
    }

    private List<ComentarioModel>Comentarios;

    public String getTipoProblema() {
        return TipoProblema;
    }

    public void setTipoProblema(String tipoProblema) {
        TipoProblema = tipoProblema;
    }

    private String TipoProblema;

    public String getTipoDenuncia() {
        return TipoDenuncia;
    }

    public void setTipoDenuncia(String tipoDenuncia) {
        TipoDenuncia = tipoDenuncia;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getPhotoURL() {
        return PhotoURL;
    }

    public void setPhotoURL(String photoURL) {
        PhotoURL = photoURL;
    }

    public double getLatitud() {
        return Latitud;
    }

    public void setLatitud(double latitud) {
        Latitud = latitud;
    }

    public double getLongitud() {
        return Longitud;
    }

    public void setLongitud(double longitud) {
        Longitud = longitud;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }


}
