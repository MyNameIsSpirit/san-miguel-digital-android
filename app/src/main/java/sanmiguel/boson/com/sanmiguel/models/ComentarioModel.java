package sanmiguel.boson.com.sanmiguel.models;

import java.util.Date;

/**
 * Created by benjamin.rivera on 21/04/2016.
 */
public class ComentarioModel {
    private String Comentario;
    private Boolean Eliminado;
    private String Fecha;
    private Integer Id;
    private String PublicadoPor;
    private Integer PublicadoId;

    public String getComentario() {
        return Comentario;
    }

    public void setComentario(String comentario) {
        Comentario = comentario;
    }

    public Boolean getEliminado() {
        return Eliminado;
    }

    public void setEliminado(Boolean eliminado) {
        Eliminado = eliminado;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String fecha) {
        Fecha = fecha;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getPublicadoPor() {
        return PublicadoPor;
    }

    public void setPublicadoPor(String publicadoPor) {
        PublicadoPor = publicadoPor;
    }

    public Integer getPublicadoId() {
        return PublicadoId;
    }

    public void setPublicadoId(Integer publicadoId) {
        PublicadoId = publicadoId;
    }




}
