package sanmiguel.boson.com.sanmiguel.functions;

import android.location.Location;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.text.format.DateUtils;

import com.loopj.android.http.SyncHttpClient;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MIME;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jarvis on 2/28/16.
 */
public class HttpGetPost {

    SyncHttpClient client;
    String BaseURL = "";

    public HttpGetPost(String url){
        client = new SyncHttpClient();
        BaseURL = url;
    }
    int timeout = (int) (30 * DateUtils.SECOND_IN_MILLIS);

    public boolean RegistrarUsuario(String email, String contrasena, int tipoDispositivo, String deviceId, String phoneNumber, String nombre, String apellido){
        List<NameValuePair> params = new ArrayList<NameValuePair>(5);
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("contrasena", contrasena));
        params.add(new BasicNameValuePair("tipoDispositivo", tipoDispositivo + ""));
        params.add(new BasicNameValuePair("deviceId", deviceId));
        params.add(new BasicNameValuePair("phoneNumber", phoneNumber));
        params.add(new BasicNameValuePair("nombre", nombre));
        params.add(new BasicNameValuePair("apellido", apellido));
        return EnviarPost(params, "Usuario/RegistrarseEmail");
    }

    public boolean IniciarSesionFacebook(String email, String contrasena, int tipoDispositivo, String deviceId, String phoneNumber, String nombre, String apellido){
        try {
            nombre = URLEncoder.encode(nombre, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            apellido = URLEncoder.encode(apellido, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        List<NameValuePair> params = new ArrayList<NameValuePair>(5);
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("contrasena", contrasena));
        params.add(new BasicNameValuePair("tipoDispositivo", tipoDispositivo + ""));
        params.add(new BasicNameValuePair("deviceId", deviceId));
        params.add(new BasicNameValuePair("phoneNumber", phoneNumber));
        params.add(new BasicNameValuePair("nombre", nombre));
        params.add(new BasicNameValuePair("apellido", apellido));
        return EnviarPost(params, "Usuario/IniciarSesionORegistrarse");
    }

    public boolean RecuperarContrasena(String email){
        List<NameValuePair> params = new ArrayList<NameValuePair>(6);
        params.add(new BasicNameValuePair("email", email));
        return EnviarPost(params, "Sesion/RestaurarContrasena");
    }

    public boolean EnviarToken(String deviceId, String token){
        List<NameValuePair> params = new ArrayList<NameValuePair>(6);
        params.add(new BasicNameValuePair("deviceId", deviceId));
        params.add(new BasicNameValuePair("token", token));
        return EnviarPost(params, "Usuario/RegistrarToken");
    }

    public void ActualizarUsuario(String nombre, String apellido, String telefono, String deviceId){
        List<NameValuePair> params = new ArrayList<NameValuePair>(6);
        params.add(new BasicNameValuePair("nombre", nombre.replaceAll(" ", "%20") ));
        params.add(new BasicNameValuePair("apellido", apellido.replaceAll(" ", "%20")));
        params.add(new BasicNameValuePair("telefono", telefono.replaceAll(" ", "%20")));
        params.add(new BasicNameValuePair("deviceId", deviceId));
        EnviarPost(params, "Usuario/ActualizarUsuario");
    }

    public boolean EnviarFormularioDenuncia(String descripcion, String direccion, Location ubicacion, String denunciaId, String deviceId, String path){

        List<NameValuePair> params = new ArrayList<NameValuePair>(6);
        params.add(new BasicNameValuePair("idTipoProblema", denunciaId));
        params.add(new BasicNameValuePair("descripcion", URLEncoder.encode(descripcion)));
        params.add(new BasicNameValuePair("direccion", URLEncoder.encode(direccion)));
        params.add(new BasicNameValuePair("latitud", URLEncoder.encode( String.valueOf(ubicacion.getLatitude()) ) ));
        params.add(new BasicNameValuePair("longitud", URLEncoder.encode(String.valueOf(ubicacion.getLongitude()) )));
        params.add(new BasicNameValuePair("idDevice", URLEncoder.encode(deviceId)));

        return EnviarPostConArchivo(params, "Denuncia/AgregarDenuncia", path);
    }

    public void EnviarDenuncia(String descripcion, String direccion, Location ubicacion, String denunciaId, String deviceId, File foto){
        String responseBody = "failure";
        HttpClient client = new DefaultHttpClient();
        client.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

        String url = BaseURL + "Denuncia/AgregarDenuncias";//WWPApi.URL_USERS;
        Map<String, String> map = new HashMap<String, String>();

        HttpPost post = new HttpPost(url);
        post.addHeader("Accept", "application/json");

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setCharset(MIME.UTF8_CHARSET);

        builder.addTextBody("idTipoProblema", denunciaId, ContentType.create("text/plain", MIME.UTF8_CHARSET));
        builder.addTextBody("descripcion", descripcion, ContentType.create("text/plain", MIME.UTF8_CHARSET));
        builder.addTextBody("direccion", direccion, ContentType.create("text/plain", MIME.UTF8_CHARSET));
        builder.addTextBody("latitud", String.valueOf(ubicacion.getLatitude()), ContentType.create("text/plain", MIME.UTF8_CHARSET));
        builder.addTextBody("longitud", String.valueOf(ubicacion.getLongitude()) , ContentType.create("text/plain", MIME.UTF8_CHARSET));
        builder.addTextBody("idDevice", deviceId, ContentType.create("text/plain", MIME.UTF8_CHARSET));
        builder.addBinaryBody("Filedata", foto, ContentType.MULTIPART_FORM_DATA, foto.getName());

        post.setEntity(builder.build());

        try {
            responseBody = EntityUtils.toString(client.execute(post).getEntity(), "UTF-8");
//  System.out.println("Response from Server ==> " + responseBody);

            JSONObject object = new JSONObject(responseBody);
            Boolean success = object.optBoolean("success");
            String message = object.optString("error");

            if (!success) {
                responseBody = message;
            } else {
                responseBody = "success";
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            client.getConnectionManager().shutdown();
        }
    }

    public boolean EnviarComentario(String comentario, String denunciaId, String deviceId){
        List<NameValuePair> params = new ArrayList<NameValuePair>(6);
        params.add(new BasicNameValuePair("idDenuncia", denunciaId));
        params.add(new BasicNameValuePair("comentario", URLEncoder.encode(comentario)));
        params.add(new BasicNameValuePair("deviceId", URLEncoder.encode(deviceId)));

        String parameters = RewriteParams(params);
        String url = BaseURL + "Denuncia/AgregarComentario" + parameters;
        new Check().execute (url ) ;

        return true;
        //return EnviarPost(params, "Comentario/Agregar");
    }

    public boolean Login(String email, String contrasena, int tipoDispositivo, String deviceId){
        List<NameValuePair> params = new ArrayList<NameValuePair>(4);
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("contrasena", contrasena));
        params.add(new BasicNameValuePair("tipoDispositivo", tipoDispositivo + ""));
        params.add(new BasicNameValuePair("deviceId", deviceId));

        HttpResponse response = EnviarPostHttpResponse(params, "Usuario/LoginEmail" );

        HttpEntity entity = response.getEntity();
        try {
            String entityresult = EntityUtils.toString(entity);
            return Boolean.valueOf(entityresult);
        } catch (IOException e) {
            return false;
        }
    }

    public String RewriteParams(List<NameValuePair> params){
        String parameters = "";
        for (NameValuePair item : params) {
            parameters+= item.getName() + "=" + item.getValue()  + "&";
        }
        return "?" + parameters;
    }

    public boolean IsLogged(String deviceId){
        List<NameValuePair> params = new ArrayList<NameValuePair>(1);
        params.add(new BasicNameValuePair("deviceId", deviceId));

        String queryString = RewriteParams(params);

        HttpResponse response = EnviarPostHttpResponse( params, "Usuario/IsLogged");

        HttpEntity entity = response.getEntity();
        try {
            String entityresult = EntityUtils.toString(entity);
            return Boolean.valueOf(entityresult);
        } catch (IOException e) {
            return false;
        }
    }


    public void CerrarSesion(String deviceId){
        List<NameValuePair> params = new ArrayList<NameValuePair>(1);
        params.add(new BasicNameValuePair("deviceId", deviceId));
        EnviarPost(params, "Usuario/CerrarSesion");
    }

    private boolean EnviarPost(List<NameValuePair> params, String url){
        String queryString = RewriteParams(params);
        url = BaseURL + url + queryString;
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);

        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            httppost.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse response = httpclient.execute(httppost);

            return Boolean.parseBoolean(EntityUtils.toString(response.getEntity()));
        } catch (ClientProtocolException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
    }

    private boolean EnviarPostConArchivo(List<NameValuePair> params, String url, String path){
        String queryString = RewriteParams(params);
        url = BaseURL + url + queryString;
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);

        try {
            httppost.setEntity(new UrlEncodedFormEntity(params));

            if(path != null && path != ""){
                MultipartEntity mpEntity = new MultipartEntity();

                File file = new File(path);
                mpEntity.addPart("avatar", new FileBody(file));
                httppost.setEntity(mpEntity);
            }


            HttpResponse response = httpclient.execute(httppost);

            return true;
        } catch (ClientProtocolException e) {
            String exception = e.getMessage();
            return false;
        } catch (IOException e) {
            String exception = e.getMessage();
            return false;
        }
    }

    private HttpResponse EnviarPostHttpResponse(List<NameValuePair> params, String url){
        String queryString = RewriteParams(params);
        url = BaseURL + url + queryString;
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url);

        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            httppost.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse response = httpclient.execute(httppost);
            return response;
        } catch (ClientProtocolException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
    }

}

class Check extends AsyncTask<String, Integer, Boolean> {

    @Override
    protected Boolean doInBackground(String... url) {

        //Add main arguments here; Connection that reffers to other methods but
        //String queryString = new HttpGetPost().RewriteParams(params);

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(url[0]);

        try {

            //httppost.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse response = httpclient.execute(httppost);

            return true;
        } catch (ClientProtocolException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
    }
}