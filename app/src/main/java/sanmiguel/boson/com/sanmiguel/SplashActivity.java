package sanmiguel.boson.com.sanmiguel;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }

    private final int SPLASH_DISPLAY_LENGTH = 2500;
    CoordinatorLayout BackgroundImage;
    int[] images = new int[] {sanmiguel.boson.com.sanmiguel.R.drawable.bg01, sanmiguel.boson.com.sanmiguel.R.drawable.bg02, sanmiguel.boson.com.sanmiguel.R.drawable.bg03, sanmiguel.boson.com.sanmiguel.R.drawable.bg04, sanmiguel.boson.com.sanmiguel.R.drawable.bg05};

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(sanmiguel.boson.com.sanmiguel.R.layout.activity_splash);

        BackgroundImage = (CoordinatorLayout)findViewById(sanmiguel.boson.com.sanmiguel.R.id.background);
        int imageId = (int)(Math.random() * images.length);
        BackgroundImage.setBackgroundResource(images[imageId]);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

}
