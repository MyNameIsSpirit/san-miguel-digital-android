package sanmiguel.boson.com.sanmiguel.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import sanmiguel.boson.com.sanmiguel.functions.DenunciasAdapter;
import sanmiguel.boson.com.sanmiguel.functions.DownloadData;
import sanmiguel.boson.com.sanmiguel.R;
import sanmiguel.boson.com.sanmiguel.models.DetalleDenunciaModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link VerDenunciasFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link VerDenunciasFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VerDenunciasFragment extends Fragment implements DownloadData.download_complete, OnMapReadyCallback {

    private OnFragmentInteractionListener mListener;
    public ArrayList<DetalleDenunciaModel> Denuncias = new ArrayList<>();
    public DenunciasAdapter adapter;
    public ListView list;

    private final LatLng LOCATION_SAN_MIGUEL = new LatLng(13.4822591,-88.177593);

    private GoogleMap googleMap;
    private MapView mapView;

    public VerDenunciasFragment() {
    }

    public static VerDenunciasFragment newInstance() {
        VerDenunciasFragment fragment = new VerDenunciasFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ver_denuncias, container, false);

        list = (ListView) view.findViewById(R.id.list);
        adapter = new DenunciasAdapter(this);
        list.setAdapter(adapter);

        mapView = (MapView) view.findViewById(R.id.map_container );
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        DownloadData download_data = new DownloadData(this);
        download_data.download_data_from_link(getContext().getString(R.string.endpoint_solicitudes_resueltas) );

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LatLng latLng = new LatLng(Denuncias.get(position).getLatitud() , Denuncias.get(position).getLongitud());
                ReCenterMap(latLng, 16, Denuncias.get(position).getTipoDenuncia());
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()  + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void get_data(String data) {
        try {
            JSONArray data_array = new JSONArray(data);

            for (int i = 0 ; i < data_array.length() ; i++)
            {
                JSONObject obj=new JSONObject(data_array.get(i).toString());
                try {
                    DetalleDenunciaModel add = new DetalleDenunciaModel();
                    add.setTipoDenuncia(obj.getString("TipoProblema"));
                    add.setDescripcion(obj.getString("Descripcion"));
                    add.setPhotoURL(obj.getString("Foto"));
                    add.setId(Integer.parseInt(obj.getString("Id")));
                    add.setLatitud(Double.parseDouble(obj.getString("Latitud")));
                    add.setLongitud(Double.parseDouble(obj.getString("Longitud")));
                    add.setFecha(obj.getString("Fecha"));

                    Denuncias.add(add);
                }catch(Exception ignored){
                }
            }
            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void ReCenterMap(LatLng latLng, Integer zoom, String label){
        if(googleMap != null)
            googleMap.clear();

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        googleMap.addMarker(new MarkerOptions().position(latLng).title(label).visible(true) ).showInfoWindow();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        ReCenterMap(LOCATION_SAN_MIGUEL, 14, "Alcaldia de San Miguel");
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
