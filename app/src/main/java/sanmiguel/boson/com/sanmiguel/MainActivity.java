package sanmiguel.boson.com.sanmiguel;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.facebook.login.LoginManager;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import sanmiguel.boson.com.sanmiguel.fragments.CrearComentarioFragment;
import sanmiguel.boson.com.sanmiguel.fragments.CrearDetalleDenunciaFragment;
import sanmiguel.boson.com.sanmiguel.fragments.DetalleDenunciaFragment;
import sanmiguel.boson.com.sanmiguel.fragments.MenuFragment;
import sanmiguel.boson.com.sanmiguel.fragments.MisDenunciasFragment;
import sanmiguel.boson.com.sanmiguel.fragments.PerfilFragment;
import sanmiguel.boson.com.sanmiguel.fragments.SeleccionarTipoDenunciaFragment;
import sanmiguel.boson.com.sanmiguel.fragments.ServiciosFragment;
import sanmiguel.boson.com.sanmiguel.fragments.VerDenunciasFragment;
import sanmiguel.boson.com.sanmiguel.functions.Functions;
import sanmiguel.boson.com.sanmiguel.functions.HttpGetPost;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        VerDenunciasFragment.OnFragmentInteractionListener, MenuFragment.OnFragmentInteractionListener,
        SeleccionarTipoDenunciaFragment.OnFragmentInteractionListener, CrearDetalleDenunciaFragment.OnFragmentInteractionListener,
        ServiciosFragment.OnFragmentInteractionListener, MisDenunciasFragment.OnFragmentInteractionListener,
        DetalleDenunciaFragment.OnFragmentInteractionListener, CrearComentarioFragment.OnFragmentInteractionListener,
        PerfilFragment.OnFragmentInteractionListener {

    Fragment fragment;
    static File filepath;
    String deviceId;
    Uri file;
    View view;
    int deep = 0;
    int branch = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(sanmiguel.boson.com.sanmiguel.R.layout.activity_main);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        //builder.detectFileUriExposure();

        deep = 0;
        Toolbar toolbar = (Toolbar) findViewById(sanmiguel.boson.com.sanmiguel.R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(sanmiguel.boson.com.sanmiguel.R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, sanmiguel.boson.com.sanmiguel.R.string.navigation_drawer_open, sanmiguel.boson.com.sanmiguel.R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        view = View.inflate(this, sanmiguel.boson.com.sanmiguel.R.layout.activity_main, null);

        NavigationView navigationView = (NavigationView) findViewById(sanmiguel.boson.com.sanmiguel.R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        getSupportActionBar().setTitle(getResources().getString(sanmiguel.boson.com.sanmiguel.R.string.app_name_upper));

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.LOCATION_HARDWARE, Manifest.permission.ACCESS_FINE_LOCATION }, 0);
            }
        }

        Intent i = getIntent();
        Bundle extras = i.getExtras();

        if(extras != null) {
            String push = extras.getString("push");
            if (push != null) {
                Integer id = Integer.parseInt(extras.getString("id"));
                goToDetalleDenuncia(id);
            }else if (  extras.getString("tag") != null  ){
                Integer id = Integer.parseInt(extras.getString("tag"));
                goToDetalleDenuncia(id);
            }else
                goToMenu();
        }else
            goToMenu();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(sanmiguel.boson.com.sanmiguel.R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == sanmiguel.boson.com.sanmiguel.R.id.nav_inicio) {
            goToMenu();
        } else if (id == sanmiguel.boson.com.sanmiguel.R.id.nav_denuncias) {
            goToDenunciasFragment(view);
        } else if (id == sanmiguel.boson.com.sanmiguel.R.id.nav_misdenuncias) {
            gotoMisDenuncias();
        } else if (id == sanmiguel.boson.com.sanmiguel.R.id.nav_perfil) {
            gotoPerfil(view);
        } else if (id == sanmiguel.boson.com.sanmiguel.R.id.nav_nuevasolicitud) {
            goToSeleccionarDenuncia(view);
        }else if (id == sanmiguel.boson.com.sanmiguel.R.id.nav_cerrarsesion) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        FirebaseInstanceId.getInstance().deleteInstanceId();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            LoginManager.getInstance().logOut();
            new HttpGetPost(getResources().getString(sanmiguel.boson.com.sanmiguel.R.string.url)).CerrarSesion(deviceId);

            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(sanmiguel.boson.com.sanmiguel.R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void goToDenunciasFragment(View view){
        deep = 1;
        branch = 1;
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        fragment = new VerDenunciasFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(sanmiguel.boson.com.sanmiguel.R.id.content, fragment).commit();
    }

    public void goToMenu(){
        deep = 0;
        branch = 0;
        fragment = new MenuFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(sanmiguel.boson.com.sanmiguel.R.id.content, fragment).commit();
    }

    public void goToSeleccionarDenuncia(View view){
        deep = 1;
        branch = 3;
        fragment = new SeleccionarTipoDenunciaFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(sanmiguel.boson.com.sanmiguel.R.id.content, fragment).commit();
        
    }

    public void gotoPerfil(View view){
        deep = 1;
        Bundle bundle = new Bundle();
        bundle.putString("deviceId", deviceId);

        fragment = new PerfilFragment();
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(sanmiguel.boson.com.sanmiguel.R.id.content, fragment).commit();
    }

    public void gotoMisDenuncias(View view){
        gotoMisDenuncias();
    }

    public void gotoMisDenuncias(){
        deep = 1;
        branch = 2;
        Bundle bundle = new Bundle();
        bundle.putString("deviceId", deviceId);
        fragment = new MisDenunciasFragment();

        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(sanmiguel.boson.com.sanmiguel.R.id.content, fragment).commit();
    }

    public void gotoDetalle(View view){
        deep = 2;
        String denunciaId = (String) view.getTag();
        gotoDetalle(denunciaId);
    }

    public void gotoDetalle(String denunciaId){
        deep = 2;
        Bundle bundle = new Bundle();
        bundle.putString("deviceId", deviceId);
        bundle.putString("denunciaId", denunciaId);
        fragment = new CrearDetalleDenunciaFragment();

        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(sanmiguel.boson.com.sanmiguel.R.id.content, fragment).commit();
         
        
    }

    public void gotoComentarioDetalle(String denunciaId){
        deep = 2;
        Bundle bundle = new Bundle();
        bundle.putString("deviceId", deviceId);
        bundle.putString("denunciaId", denunciaId);
        fragment = new CrearComentarioFragment();

        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(sanmiguel.boson.com.sanmiguel.R.id.content, fragment).commit();
    }

    public void goToDetalleDenuncia(Integer id){
        deep = 2;
        Bundle bundle = new Bundle();
        bundle.putString("id", id.toString());
        fragment = new DetalleDenunciaFragment();

        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(sanmiguel.boson.com.sanmiguel.R.id.content, fragment).commit();
         fragmentManager.executePendingTransactions();
    }

    public void goToStartCamera(View view){
        goToStartCamera();
    }

    public void goToStartCamera(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
        startActivityForResult(intent, 100);
    }

    private static File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "CameraDemo");

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        filepath = new File(mediaStorageDir.getPath() + File.separator + "IMG_"+ timeStamp + ".jpg");
        return new File(mediaStorageDir.getPath() + File.separator + "IMG_"+ timeStamp + ".jpg");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {

                ImageButton btnTakeSnap;
                btnTakeSnap = (ImageButton) findViewById(sanmiguel.boson.com.sanmiguel.R.id.btnTakeSnap);
                btnTakeSnap.setImageURI(file);
                new Functions().saveBitmapToFile(filepath);
                btnTakeSnap.setTag(filepath);
            }
        }
    }

    public void goToServiciosFragment(View view){
        deep = 1;
        fragment = new ServiciosFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(sanmiguel.boson.com.sanmiguel.R.id.content, fragment).commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }


    @Override
    public void onBackPressed() {
        if(deep == 1)
            goToMenu();
        else if(deep == 2){
            if(branch == 1)
                goToDenunciasFragment(view);
            else if(branch == 2)
                gotoMisDenuncias();
            else if(branch == 3)
                goToSeleccionarDenuncia(view);
            else goToMenu();
        }
        else
            super.onBackPressed();
    }


}