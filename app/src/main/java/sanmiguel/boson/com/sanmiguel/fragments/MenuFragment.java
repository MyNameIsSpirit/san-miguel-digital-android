package sanmiguel.boson.com.sanmiguel.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.SimpleShowcaseEventListener;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import sanmiguel.boson.com.sanmiguel.MainActivity;
import sanmiguel.boson.com.sanmiguel.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MenuFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MenuFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match

    ImageView btnDenunciasResueltas;
    ImageView btnNuevaDenuncia;
    ImageView btnMisSolicitudes;
    ImageView btnServicioLinea;

    private ShowcaseView showcaseView;
    private int counter = 0;

    View view;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public MenuFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MenuFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MenuFragment newInstance(String param1, String param2) {
        MenuFragment fragment = new MenuFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_menu, container, false);

        btnDenunciasResueltas = (ImageView)view.findViewById(R.id.btnDenunciasResueltas);
        btnNuevaDenuncia = (ImageView)view.findViewById(R.id.btnNuevaDenuncia);
        btnMisSolicitudes = (ImageView)view.findViewById(R.id.btnMisSolicitudes);
        btnServicioLinea = (ImageView)view.findViewById(R.id.btnServicioLinea);

        showcaseView = new ShowcaseView.Builder((MainActivity)getContext())
                .setTarget(com.github.amlcurran.showcaseview.targets.Target.NONE)
                .singleShot(42)
                .withMaterialShowcase()
                .setContentTitle(getResources().getString(R.string.app_name_upper))
                .setContentText(getResources().getString(R.string.instruccion_menu_principal))
                .setStyle(R.style.CustomTheme)
                .setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        switch (counter) {
                            case 0:
                                showcaseView.setShowcase(new ViewTarget(btnDenunciasResueltas), true);
                                showcaseView.setContentTitle(getResources().getString(R.string.menu_principal_option_1));
                                showcaseView.setContentText(getResources().getString(R.string.instruccion_menu_principal_opcion_1));
                                break;

                            case 1:
                                showcaseView.setShowcase(new ViewTarget(btnNuevaDenuncia), true);
                                showcaseView.setContentTitle(getResources().getString(R.string.menu_principal_option_2));
                                showcaseView.setContentText(getResources().getString(R.string.instruccion_menu_principal_opcion_2));
                                break;

                            case 2:
                                showcaseView.setShowcase(new ViewTarget(btnMisSolicitudes), true);
                                showcaseView.setContentTitle(getResources().getString(R.string.menu_principal_option_3));
                                showcaseView.setContentText(getResources().getString(R.string.instruccion_menu_principal_opcion_3));
                                break;

                            case 3:
                                showcaseView.setShowcase(new ViewTarget(btnServicioLinea), true);
                                showcaseView.setContentTitle(getResources().getString(R.string.menu_principal_option_4));
                                showcaseView.setContentText(getResources().getString(R.string.instruccion_menu_principal_opcion_4));
                                break;

                            case 4:
                                showcaseView.setTarget(com.github.amlcurran.showcaseview.targets.Target.NONE);
                                showcaseView.setContentTitle(getResources().getString(R.string.app_name));
                                showcaseView.setContentText(getResources().getString(R.string.texto_slogan));
                                showcaseView.setButtonText(getString(R.string.texto_finalizar));
                                setAlpha(0.4f, btnDenunciasResueltas, btnNuevaDenuncia, btnMisSolicitudes, btnServicioLinea);
                                break;

                            case 5:
                                showcaseView.hide();
                                setAlpha(1.0f, btnDenunciasResueltas, btnNuevaDenuncia, btnMisSolicitudes, btnServicioLinea);
                                break;
                        }
                        counter++;
                    }
                })
                .build();
        showcaseView.setButtonText(getString(R.string.texto_siguiente));

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private void setAlpha(float alpha, View... views) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            for (View view : views) {
                view.setAlpha(alpha);
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
