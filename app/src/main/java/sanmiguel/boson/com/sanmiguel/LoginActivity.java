package sanmiguel.boson.com.sanmiguel;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import sanmiguel.boson.com.sanmiguel.functions.Functions;
import sanmiguel.boson.com.sanmiguel.functions.HttpGetPost;

public class LoginActivity extends AppCompatActivity {
    Button BtnIniciar;
    TextView BtnRecuperar;
    EditText txtCorreo;
    EditText txtContrasena;
    String androidId;
    TextView btnRegistrar;
    LoginButton loginButton;
    CallbackManager callbackManager;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());// openRequest.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);

        //authButton.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);



        setContentView(sanmiguel.boson.com.sanmiguel.R.layout.activity_login);

        context = this;

        BtnIniciar = (Button) findViewById(sanmiguel.boson.com.sanmiguel.R.id.Iniciar);
        BtnRecuperar = (TextView) findViewById(sanmiguel.boson.com.sanmiguel.R.id.btnRecuperar);

        txtCorreo = (EditText) findViewById(sanmiguel.boson.com.sanmiguel.R.id.txtCorreo);
        txtContrasena = (EditText) findViewById(sanmiguel.boson.com.sanmiguel.R.id.txtContrasena);
        btnRegistrar = (TextView) findViewById(sanmiguel.boson.com.sanmiguel.R.id.btnRegistrar );

        if(!isOnline()){
            new AlertDialog.Builder((LoginActivity.this ))
                    .setTitle(getString(sanmiguel.boson.com.sanmiguel.R.string.app_name))
                    .setMessage(getString(sanmiguel.boson.com.sanmiguel.R.string.mensaje_error_conexion))
                    .setCancelable(false)
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            isOnline();
                        }
                    }).create().show();
            return;
        }

        androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        Boolean isLogged = new HttpGetPost(getResources().getString(sanmiguel.boson.com.sanmiguel.R.string.url)).IsLogged(androidId);

        if(isLogged){
            Intent intent = new Intent( LoginActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        BtnIniciar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean result = new HttpGetPost(getResources().getString(sanmiguel.boson.com.sanmiguel.R.string.url)).Login( txtCorreo.getText().toString() , txtContrasena.getText().toString(),2, androidId );
                if(result){
                    String recentToken = FirebaseInstanceId.getInstance().getToken();
                    String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                    new HttpGetPost(getResources().getString(sanmiguel.boson.com.sanmiguel.R.string.url)).EnviarToken(deviceId, recentToken);

                    Intent intent = new Intent( v.getContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }else{
                    Toast.makeText(v.getContext(), getString(sanmiguel.boson.com.sanmiguel.R.string.mensaje_error_inicio_sesion), Toast.LENGTH_LONG).show();
                }
            }
        });

        BtnRecuperar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( v.getContext(), RecuperarContrasena.class);
                startActivity(intent);
            }
        });

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), RegistroActivity.class);
                startActivity(intent);
            }
        });

        MultiDex.install(this);

        loginButton = (LoginButton) findViewById(sanmiguel.boson.com.sanmiguel.R.id.login_button);

        loginButton.setLoginBehavior(LoginBehavior.SUPPRESS_SSO);


        loginButton.setReadPermissions("email");

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        try {
                                            String email = object.getString("email");
                                            String name = object.getString("name");
                                            String[] nombres = new Functions().FormatearNombre(name);
                                            UUID uuid = UUID.randomUUID();
                                            String password = uuid.toString();
                                            boolean result = new HttpGetPost(getResources().getString(sanmiguel.boson.com.sanmiguel.R.string.url)).IniciarSesionFacebook(email, password, 1, androidId, "", nombres[0], nombres[1]);

                                            if(result){
                                                String recentToken = FirebaseInstanceId.getInstance().getToken();
                                                String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                                                new HttpGetPost(getResources().getString(sanmiguel.boson.com.sanmiguel.R.string.url)).EnviarToken(deviceId, recentToken);

                                                Intent intent = new Intent( getApplicationContext() , MainActivity.class);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);
                                            }else{
                                                Toast.makeText(getApplicationContext(), getString(sanmiguel.boson.com.sanmiguel.R.string.mensaje_error_inicio_sesion_facebook), Toast.LENGTH_LONG).show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(context, getString(sanmiguel.boson.com.sanmiguel.R.string.mensaje_error_inicio_sesion_facebook), Toast.LENGTH_LONG).show();

                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }
}
