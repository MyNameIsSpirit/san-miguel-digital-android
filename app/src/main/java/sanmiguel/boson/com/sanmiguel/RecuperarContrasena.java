package sanmiguel.boson.com.sanmiguel;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class RecuperarContrasena extends AppCompatActivity {

    EditText txtCorreo;
    Button BtnIniciar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(sanmiguel.boson.com.sanmiguel.R.layout.activity_recuperar_contrasena);
        Toolbar toolbar = (Toolbar) findViewById(sanmiguel.boson.com.sanmiguel.R.id.toolbar);
        setSupportActionBar(toolbar);

        txtCorreo = (EditText) findViewById(sanmiguel.boson.com.sanmiguel.R.id.txtCorreo);
        BtnIniciar = (Button) findViewById(sanmiguel.boson.com.sanmiguel.R.id. Registrar);

        BtnIniciar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( !txtCorreo.getText().toString().matches("") ){
                    String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                    //boolean result = new HttpGetPost().RecuperarContrasena(txtCorreo.getText().toString());
                    String url = "http://sanmiguel.gob.sv/alcaldiamovil/Usuario/OlvideContrasena?correo=" +  txtCorreo.getText().toString();

                    DefaultHttpClient httpclient = new DefaultHttpClient(new BasicHttpParams());
                    HttpPost httppost = new HttpPost(url);
// Depends on your web service
                    httppost.setHeader("Content-type", "application/json");

                    InputStream inputStream = null;
                    String result = null;
                    try {
                        HttpResponse response = httpclient.execute(httppost);
                        HttpEntity entity = response.getEntity();

                        inputStream = entity.getContent();
                        // json is UTF-8 by default
                        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
                        StringBuilder sb = new StringBuilder();

                        String line = null;
                        while ((line = reader.readLine()) != null)
                        {
                            sb.append(line + "\n");
                        }
                        result = sb.toString();
                        JSONObject jObject = new JSONObject(result);
                        boolean valid = jObject.getBoolean("resultado");

                        if(valid){
                            Toast.makeText(v.getContext(), "Se ha enviado una correo de recuperacion de contrasena.", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent( RecuperarContrasena.this, LoginActivity.class);
                            startActivity(intent);
                        }else{
                            String mensaje = jObject.getString("mensaje");
                            Toast.makeText(v.getContext(), mensaje, Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        // Oops
                        Toast.makeText(v.getContext(), "Ha ocurrido un error inesperado mientras se solicitaba el cambio de contraseña.", Toast.LENGTH_LONG).show();
                    }
                    finally {
                        try{if(inputStream != null)inputStream.close();}catch(Exception squish){}
                    }


                    /*if(result){
                        Toast.makeText(v.getContext(), "Se ha enviado una correo de recuperacion de contrasena.", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent( RecuperarContrasena.this, LoginActivity.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(v.getContext(), "El correo electronico no se encuentra registrado.", Toast.LENGTH_LONG).show();
                    }*/
                }else{
                    Toast.makeText(v.getContext(), "Debe ingresar un correo electronico.", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }

  }

