package sanmiguel.boson.com.sanmiguel;

import android.provider.Settings;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import sanmiguel.boson.com.sanmiguel.functions.HttpGetPost;

/**
 * Created by Jarvis on 1/2/17.
 */
public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {

        String recentToken = FirebaseInstanceId.getInstance().getToken();
        String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        new HttpGetPost(getResources().getString(sanmiguel.boson.com.sanmiguel.R.string.url)).EnviarToken(deviceId, recentToken);

        Log.d("REG_TOKEN", recentToken);
    }
}
