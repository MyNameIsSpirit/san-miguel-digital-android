package sanmiguel.boson.com.sanmiguel.functions;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 * Created by Jarvis on 9/8/16.
 */
public  class Functions {

    public  boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    public String[] FormatearNombre(String nombre){
        String[] nombres = nombre.split("\\s+");
        if(nombres.length == 0)
            return new String[]{"", ""};
        if(nombres.length == 1)
            return new String[]{nombres[0], ""};
        if(nombres.length == 2)
            return  new String[]{nombres[0], nombres[1]};

        String _nombre = "";
        String _apellido = "";
        for(int i = 0; i < nombres.length; i++){
            if(  i < (nombres .length / 2) ){
                _nombre = _nombre + " " + nombres[i];
            }else{
                _apellido = _apellido + " " + nombres[i];
            }
        }

        return new String[]{_nombre, _apellido};
    }

    public File saveBitmapToFile(File file){
        try {

            ExifInterface exifInterface = new ExifInterface(file.getAbsolutePath());
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            Bitmap correctedBitMap = null;

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE = 75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            switch(orientation) {

                case ExifInterface.ORIENTATION_ROTATE_90:
                    correctedBitMap = rotateImage(selectedBitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    correctedBitMap = rotateImage(selectedBitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    correctedBitMap = rotateImage(selectedBitmap, 270);
                    break;

                default:
                    correctedBitMap = rotateImage(selectedBitmap, 0);
            }

            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            correctedBitMap.compress(Bitmap.CompressFormat.JPEG, 70 , outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }

    private Bitmap rotateImage(Bitmap source, float angle) {

        Bitmap bitmap = null;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        try {
            bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                    matrix, true);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
        return bitmap;
    }


}
