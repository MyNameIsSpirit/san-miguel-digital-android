package sanmiguel.boson.com.sanmiguel;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import sanmiguel.boson.com.sanmiguel.functions.Functions;
import sanmiguel.boson.com.sanmiguel.functions.HttpGetPost;

public class RegistroActivity extends AppCompatActivity {

    EditText txtEmail;
    EditText txtContrasena;
    EditText txtTelefono;
    EditText txtNombre;
    EditText txtApellido;
    Button BtnRegistro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(sanmiguel.boson.com.sanmiguel.R.layout.activity_registro);

        txtEmail = (EditText) findViewById(sanmiguel.boson.com.sanmiguel.R.id.txtCorreo);
        txtContrasena = (EditText) findViewById(sanmiguel.boson.com.sanmiguel.R.id.txtContrasena);
        txtTelefono = (EditText) findViewById(sanmiguel.boson.com.sanmiguel.R.id.txtTelefono);
        txtNombre = (EditText) findViewById(sanmiguel.boson.com.sanmiguel.R.id.txtNombre);
        txtApellido = (EditText) findViewById(sanmiguel.boson.com.sanmiguel.R.id.txtApellido);
        BtnRegistro = (Button) findViewById(sanmiguel.boson.com.sanmiguel.R.id.Register);

        txtTelefono.setText("");

        BtnRegistro.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( !txtEmail.getText().toString().matches("") || !txtContrasena.getText().toString().matches("") || !txtNombre.getText().toString().matches("") || !txtApellido.getText().toString().matches("")      ) {

                    if (new Functions().isValidEmailAddress(txtEmail.getText().toString())) {
                        String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                        boolean result = new HttpGetPost(getResources().getString(sanmiguel.boson.com.sanmiguel.R.string.url)).RegistrarUsuario(txtEmail.getText().toString(),
                                txtContrasena.getText().toString(), 2, deviceId, txtTelefono.getText().toString(), txtNombre.getText().toString(), txtApellido.getText().toString());

                        if (result) {
                            String recentToken = FirebaseInstanceId.getInstance().getToken();
                            //String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                            new HttpGetPost(getResources().getString(sanmiguel.boson.com.sanmiguel.R.string.url)).EnviarToken(deviceId, recentToken);

                            Intent intent = new Intent(RegistroActivity.this, MainActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(v.getContext(), "El usuario ya se encuentra registrado.", Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(v.getContext(), "Debe ingresar un correo electronico válido.", Toast.LENGTH_LONG).show();
                    }

                }else{
                    Toast.makeText(v.getContext(), "Debe completar todos los campos.", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent( RegistroActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }

}
