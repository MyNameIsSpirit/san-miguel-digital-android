package sanmiguel.boson.com.sanmiguel.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import sanmiguel.boson.com.sanmiguel.MainActivity;
import sanmiguel.boson.com.sanmiguel.functions.DownloadData;
import sanmiguel.boson.com.sanmiguel.functions.HttpGetPost;
import sanmiguel.boson.com.sanmiguel.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PerfilFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PerfilFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PerfilFragment extends Fragment implements DownloadData.download_complete {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Button btnActualizar;
    EditText txtNombre;
    EditText txtApellido;
    EditText txtTelefono;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public PerfilFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PerfilFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PerfilFragment newInstance(String param1, String param2) {
        PerfilFragment fragment = new PerfilFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_perfil, container, false);

        Bundle data = getArguments();
        final String androidId = data.getString("deviceId");
        btnActualizar = (Button)view.findViewById(R.id.btnActualizar);
        txtNombre = (EditText)view.findViewById(R.id.txtNombre);
        txtApellido = (EditText)view.findViewById(R.id.txtApellido);
        txtTelefono = (EditText)view.findViewById(R.id.txtTelefono);

        DownloadData download_data = new DownloadData((DownloadData.download_complete) this);
        download_data.download_data_from_link(getContext().getString(R.string.endpoint_obtener_perfil) + androidId);

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new HttpGetPost(getResources().getString(R.string.url)).ActualizarUsuario(txtNombre.getText().toString(), txtApellido.getText().toString(), txtTelefono.getText().toString(), androidId);
                Toast.makeText(getContext(), "Sus datos han sido actualizados.", Toast.LENGTH_LONG).show();
                ((MainActivity) getContext()).goToMenu();
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void get_data(String data) {
        try {
            JSONObject base = new JSONObject(data);

            if(base.getString("Nombre") != "null")
                txtNombre.setText(base.getString("Nombre"));

            if(base.getString("Apellido") != "null")
                txtApellido.setText(base.getString("Apellido"));

            if(base.getString("Telefono") != "null")
                txtTelefono.setText(base.getString("Telefono"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
