package sanmiguel.boson.com.sanmiguel.models;

/**
 * Created by Jarvis on 2/27/16.
 */
public class UsuarioInfoModel {
    private int Id;
    public String Nombre;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }
}
