package sanmiguel.boson.com.sanmiguel.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.Target;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.google.android.gms.location.LocationListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import sanmiguel.boson.com.sanmiguel.MainActivity;
import sanmiguel.boson.com.sanmiguel.functions.DownloadData;
import sanmiguel.boson.com.sanmiguel.functions.GPSTracker;
import sanmiguel.boson.com.sanmiguel.functions.HttpGetPost;
import sanmiguel.boson.com.sanmiguel.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CrearDetalleDenunciaFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CrearDetalleDenunciaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CrearDetalleDenunciaFragment extends Fragment implements LocationListener, DownloadData.download_complete {

    private OnFragmentInteractionListener mListener;
    View view;
    private ImageView btnTakeSnap;
    private Button btnSendForm;
    private EditText direccion;
    private EditText description;
    GPSTracker gps;
    private String androidId;
    private String denunciaId;
    Spinner cbxservicios;
    private boolean solicitudValida = true;
    String address= "";

    private ShowcaseView showcaseView;
    private int counter = 0;

    double latitude = 0.0;
    double longitude = 0.0;

    public CrearDetalleDenunciaFragment() {
        // Required empty public constructor
    }

    public static CrearDetalleDenunciaFragment newInstance() {
        CrearDetalleDenunciaFragment fragment = new CrearDetalleDenunciaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_crear_detalle_denuncia, container, false);

        Bundle data = getArguments();
        androidId = data.getString("deviceId");
        denunciaId = data.getString("denunciaId");
        btnTakeSnap = (ImageView)view.findViewById(R.id.btnTakeSnap);
        btnSendForm = (Button)view.findViewById(R.id.btnSendForm);
        description = (EditText)view.findViewById(R.id.description);
        direccion = (EditText)view.findViewById(R.id.direccion);
        cbxservicios = (Spinner)view.findViewById(R.id.cbxservicios);

        showcaseView = new ShowcaseView.Builder((MainActivity)getContext())
                .setTarget(Target.NONE)
                .withMaterialShowcase()
                .hideOnTouchOutside()
                .setContentTitle(getResources().getString(R.string.app_name_upper))
                .setContentText(getResources().getString(R.string.instrucion_publicacion_principal))
                .setStyle(R.style.CustomTheme)
                .setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        switch (counter) {
                            case 0:
                                showcaseView.setShowcase(new ViewTarget(cbxservicios), true);
                                showcaseView.setContentTitle(getResources().getString(R.string.publicacion_instruccion_1));
                                showcaseView.setContentText(getResources().getString(R.string.instruccion_publicacion_instruccion_1));
                                break;

                            case 1:
                                showcaseView.setShowcase(new ViewTarget(description), true);
                                showcaseView.setContentTitle(getResources().getString(R.string.publicacion_instruccion_2));
                                showcaseView.setContentText(getResources().getString(R.string.instruccion_publicacion_instruccion_2));
                                break;

                            case 2:
                                showcaseView.setShowcase(new ViewTarget(direccion), true);
                                showcaseView.setContentTitle(getResources().getString(R.string.publicacion_instruccion_3));
                                showcaseView.setContentText(getResources().getString(R.string.instruccion_publicacion_instruccion_3));
                                break;

                            case 3:
                                showcaseView.setShowcase(new ViewTarget(btnTakeSnap), true);
                                showcaseView.setContentTitle(getResources().getString(R.string.publicacion_instruccion_4));
                                showcaseView.setContentText(getResources().getString(R.string.instruccion_publicacion_instruccion_4));
                                break;

                            case 4:
                                showcaseView.setTarget(com.github.amlcurran.showcaseview.targets.Target.NONE);
                                showcaseView.setContentTitle(getResources().getString(R.string.app_name));
                                showcaseView.setContentText(getResources().getString(R.string.texto_slogan));
                                showcaseView.setButtonText(getString(R.string.texto_finalizar));
                                setAlpha(0.4f, cbxservicios, description, direccion, btnTakeSnap);
                                break;

                            case 5:
                                showcaseView.hide();
                                setAlpha(1.0f, description, direccion, cbxservicios, btnTakeSnap);
                                break;
                        }
                        counter++;
                    }
                })
                .build();
        showcaseView.setButtonText(getString(R.string.texto_siguiente));

        getLastBestLocation();

        DownloadData download_data = new DownloadData(this);
        download_data.download_data_from_link(getContext().getString(R.string.endpoint_obtener_departamentos) + denunciaId);

        return view;
    }

    private void setAlpha(float alpha, View... views) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            for (View view : views) {
                view.setAlpha(alpha);
            }
        }
    }

    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);

        btnSendForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(direccion.getText() == null || direccion.getText().equals("")  || direccion.getText().length() == 0   ){
                    new AlertDialog.Builder(getContext())
                            .setTitle(getContext().getString(R.string.app_name_upper))
                            .setMessage(getContext().getString(R.string.mensaje_error_direccion))
                            .setCancelable(false)
                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getLastBestLocation();
                                }
                            }).create().show();
                    return;
                }

                if(description.getText() == null || description.getText().equals("")  || description.getText().length() == 0 ){
                    new AlertDialog.Builder(getContext())
                            .setTitle(getContext().getString(R.string.app_name_upper))
                            .setMessage(getContext().getString(R.string.mensaje_error_descripcion))
                            .setCancelable(false)
                            .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getLastBestLocation();
                                }
                            }).create().show();
                    return;
                }

                //btnSendForm.setVisibility(v.GONE);
                final Location location = getLastBestLocation();
                final String address= GetAddress(location.getLatitude(), location.getLongitude());
                final String snap = (btnTakeSnap.getTag() == null) ? null : btnTakeSnap.getTag().toString();

                if(solicitudValida) {

                    if(snap == null){
                        new AlertDialog.Builder((MainActivity) getContext())
                                .setTitle(getResources().getString(R.string.app_name))
                                .setMessage(getContext().getString(R.string.instruccion_dialogo_fotografía))
                                .setCancelable(false)
                                .setPositiveButton(getContext().getString(R.string.intruccion_dialogo_fotografia_positivo), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        new HttpGetPost(getResources().getString(R.string.url)).EnviarFormularioDenuncia(description.getText().toString(), direccion.getText() + " - (Dirección calculada por dispositivo móvil: " + address + ")", location, denunciaId,  androidId, snap );
                                        Toast.makeText(getContext(), getContext().getString(R.string.mensaje_envio_solicitud), Toast.LENGTH_LONG).show();

                                        ((MainActivity) getContext()).goToMenu();
                                    }
                                })
                                .setNegativeButton(getContext().getString(R.string.intruccion_dialogo_fotografia_negativo), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .create().show();
                    }else{
                        new HttpGetPost(getResources().getString(R.string.url)).EnviarFormularioDenuncia(description.getText().toString(), direccion.getText() + " - (Dirección calculada por dispositivo móvil: " + address + ")", location, denunciaId,  androidId, snap );
                        Toast.makeText(getContext(), getContext().getString(R.string.mensaje_envio_solicitud), Toast.LENGTH_LONG).show();

                        ((MainActivity) getContext()).goToMenu();
                    }

                }

            }
        });
    }

    private Location getLastBestLocation() {
        gps = new GPSTracker(getContext());

        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            address = GetAddress(latitude, longitude);

            if(latitude == 0 || longitude == 0) {
                return getLastBestLocation();
            }

            Toast.makeText(getContext(), getContext().getString(R.string.mensaje_iniciando_servicio_GPS), Toast.LENGTH_SHORT).show();
            return gps.getLocation();
        } else {

            new AlertDialog.Builder((MainActivity) getContext())
                    .setTitle(getResources().getString(R.string.app_name))
                    .setMessage(getContext().getString(R.string.mensaje_activar_GPS))
                    .setCancelable(false)
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getLastBestLocation();
                        }
                    }).create().show();

        }
        return null;
    }

    public void CiudadFalsa(String ciudad){
        btnSendForm.setVisibility(View.GONE);
        solicitudValida = false;
        new AlertDialog.Builder(getContext())
                .setTitle(getContext().getString(R.string.app_name_upper))
                .setMessage(getContext().getString(R.string.mensaje_ubicacion_fuera_area) +  " " + ciudad)
                .setCancelable(false)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Whatever...
                    }
                }).show();
    }

    public String GetAddress(double latitude, double longitude){
        if(latitude  == 0 || longitude == 0){
            //Toast.makeText(getContext(), "Es necesario verificar su conexión", Toast.LENGTH_LONG).show();
            return "";
        }
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getContext(), Locale.getDefault());
        String direccion = "";

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            geocoder.getFromLocation(latitude, longitude, 1);
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();

            String localidad = addresses.get(0).getSubLocality();

            if(address == null)
                address = "";
            if(localidad == null)
                localidad = "";
            if(city == null)
                city = "";
            if(state == null)
                state = "";

            if(city != null){
                if(!city.toUpperCase().equals(getContext().getString(R.string.ubicacion))) {
                    CiudadFalsa(city);
                }
            }

            direccion =  address  + " " + localidad + " " + city +" " + state;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return direccion;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void sendForm(View view){
        sendForm();
    }

    public void sendForm(){

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void get_data(String data) {
        //cbxservicios
        List<String> spinnerArray = new ArrayList<>();
        try {
            //JSONObject base = new JSONObject(data);
            JSONArray data_array = new JSONArray(data);

            for (int i = 0 ; i < data_array.length() ; i++)
            {
                JSONObject obj=new JSONObject(data_array.get(i).toString());
                try {
                    spinnerArray.add(obj.getString("Value"));
                }catch(Exception ignored){
                }
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_spinner_item, spinnerArray);
            adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            cbxservicios.setAdapter(adapter);
            cbxservicios.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
