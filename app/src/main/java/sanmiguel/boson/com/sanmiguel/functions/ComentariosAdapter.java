package sanmiguel.boson.com.sanmiguel.functions;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;

import sanmiguel.boson.com.sanmiguel.MainActivity;
import sanmiguel.boson.com.sanmiguel.R;
import sanmiguel.boson.com.sanmiguel.fragments.DetalleDenunciaFragment;
import sanmiguel.boson.com.sanmiguel.models.ComentarioModel;

/**
 * Created by benjamin.rivera on 21/04/2016.
 */
public class ComentariosAdapter extends BaseAdapter {

    DetalleDenunciaFragment main;

    public ComentariosAdapter(DetalleDenunciaFragment main){
        this.main = main;
    }
    @Override
    public int getCount() {
        return  main.Comentarios.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static class ViewHolderItem {
        TextView comentario;
        TextView fecha;
        TextView por;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        ViewHolderItem holder = new ViewHolderItem();
        if (convertView == null) {
            MainActivity activity = (MainActivity) main.getContext();

            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.cell_comentarios, null);

            holder.comentario = (TextView) convertView.findViewById(R.id.comentario);
            holder.fecha = (TextView) convertView.findViewById(R.id.fecha);
            holder.por = (TextView) convertView.findViewById(R.id.por);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolderItem) convertView.getTag();
        }

        ComentarioModel current = this.main.Comentarios.get(position);

        holder.comentario.setText(current.getComentario());
        holder.fecha.setText(current.getFecha());
        holder.por.setText(current.getPublicadoPor());

        return convertView;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
