package sanmiguel.boson.com.sanmiguel.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import sanmiguel.boson.com.sanmiguel.MainActivity;
import sanmiguel.boson.com.sanmiguel.functions.ComentariosAdapter;
import sanmiguel.boson.com.sanmiguel.functions.DownloadData;
import sanmiguel.boson.com.sanmiguel.R;
import sanmiguel.boson.com.sanmiguel.models.ComentarioModel;
import sanmiguel.boson.com.sanmiguel.models.DetalleDenunciaModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DetalleDenunciaFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DetalleDenunciaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetalleDenunciaFragment extends Fragment  implements DownloadData.download_complete,
        CrearComentarioFragment.OnFragmentInteractionListener, OnMapReadyCallback {

    private final LatLng LOCATION_SAN_MIGUEL = new LatLng(13.4822591,-88.177593);

    public ArrayList<ComentarioModel> Comentarios = new ArrayList<>();

    private GoogleMap googleMap;
    private MapView mapView;

    private String id;
    public ComentariosAdapter adapter;
    private OnFragmentInteractionListener mListener;

    public ListView list;
    TextView TxtTipoProblema;
    TextView TxtComentario;
    private Button btnComentar;
    View view;
    ImageView imageView2;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetalleDenunciaFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetalleDenunciaFragment newInstance(String param1, String param2) {
        DetalleDenunciaFragment fragment = new DetalleDenunciaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_detalle_denuncia, container, false);

        Bundle data = getArguments();
        id = data.getString("id");

        TxtTipoProblema = (TextView) view.findViewById(R.id.tipoproblema);
        TxtComentario = (TextView) view.findViewById(R.id.comentario);
        imageView2 = (ImageView) view.findViewById(R.id.imageView2);

        list = (ListView) view.findViewById(R.id.list);
        adapter = new ComentariosAdapter(this);
        list.setAdapter(adapter);

        mapView = (MapView) view.findViewById(R.id.map_container );
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        DownloadData download_data = new DownloadData(this);
        download_data.download_data_from_link(getContext().getString(R.string.endpoint_detalle_solicitud) + id);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, "La Alcaldia Municipal de San Miguel, trabajando para ti.  http://www.sanmiguel.gob.sv/");
                PackageManager pm = v.getContext().getPackageManager();
                List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
                for (final ResolveInfo app : activityList) {
                    if ((app.activityInfo.name).contains("facebook")) {
                        final ActivityInfo activity = app.activityInfo;
                        final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
                        shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
                        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                        shareIntent.setComponent(name);
                        v.getContext().startActivity(shareIntent);
                        break;
                    }
                }

            }
        });

        btnComentar = (Button)view.findViewById(R.id.btnComentar);
        btnComentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getContext()).gotoComentarioDetalle(id);
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void get_data(String data) {
        try {
            JSONObject base = new JSONObject(data);
            DetalleDenunciaModel denuncia = new DetalleDenunciaModel();
            denuncia.setTipoProblema(base.getString("TipoProblema").toUpperCase());
            denuncia.setDescripcion(base.getString("Descripcion"));

            String etapa = base.getString("Etapa");

            TxtTipoProblema.setText(denuncia.getTipoProblema());
            TxtComentario.setText(denuncia.getDescripcion());

            TxtComentario.post(new Runnable() {
                @Override
                public void run() {
                    int height_in_pixels = TxtComentario.getLineCount() * (TxtComentario.getLineHeight() + 1);
                    TxtComentario.setHeight(height_in_pixels);
                }
            });


            String url = base.getString("Foto").toString(); //BaseURL + id + ".jpg";
            new DownloadImageTask(imageView2).execute(url);

            if(etapa.equals("DENEGADA") || etapa.equals("TRABAJO FINALIZADO") ){
                btnComentar.setVisibility(   View.INVISIBLE);
            }

            double latitude = Double.parseDouble(base.getString("Latitud"));
            double longitude = Double.parseDouble(base.getString("Longitud"));

            LatLng latLng = new LatLng(latitude,longitude);
            ReCenterMap(latLng, 16, denuncia.getTipoProblema());

            JSONArray data_array = new JSONArray(  base.getString("Comentarios")  );

            for (int i = 0 ; i < data_array.length() ; i++)
            {
                JSONObject obj=new JSONObject(data_array.get(i).toString());
                try {
                    ComentarioModel add = new ComentarioModel();
                    add.setId(Integer.parseInt(obj.getString("Id")));
                    add.setComentario(obj.getString("Comentario"));
                    add.setPublicadoPor(obj.getString("PublicadoPor"));
                    add.setFecha(obj.getString("Fecha"));
                    Comentarios.add(add);
                }catch(Exception ex){
                    ex.printStackTrace();
                }
            }
            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void ReCenterMap(LatLng latLng, Integer zoom, String label){
        if(googleMap != null)
            googleMap.clear();

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        googleMap.addMarker(new MarkerOptions().position(latLng).title(label).visible(true) ).showInfoWindow();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        ReCenterMap(LOCATION_SAN_MIGUEL, 14, "Alcaldia de San Miguel");
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}


class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;

    public DownloadImageTask(ImageView bmImage) {
        this.bmImage = bmImage;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        if(result != null){
            bmImage.setBackground(null);
            bmImage.setImageBitmap(result);
        }

    }
}


